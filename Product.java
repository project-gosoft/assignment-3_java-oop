package com.buzzfreeze.oop;

public class Product implements IProductFrontPage {
	public String price;
	public String title;
	public int id;

	public Product(String price, String title, int id) {
		this.price = price;
		this.title = title;
		this.id = id;

	}

	@Override
	public String toString() {
		// เพื่อเวลาเราให้มันแสดงค่าของข้อมูลที่อยู่ใน arrayList แล้วให้มันส่งค่าของ title ออกไปให้
		return title;
	}

	@Override
	public String getTitle() {
		// TODO Auto-generated method stub
		return title;
	}

	@Override
	public String getPrice() {
		// TODO Auto-generated method stub
		return price;
	}

	@Override
	public int getId() {
		// TODO Auto-generated method stub
		return id;
	}

}
