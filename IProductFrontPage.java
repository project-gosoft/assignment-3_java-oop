package com.buzzfreeze.oop;

public interface IProductFrontPage {
	public String getTitle();

	public String getPrice();

	public int getId();

}
