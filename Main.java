package com.buzzfreeze.oop;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {

	static ArrayList<Product> computer = new ArrayList<>();

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		// create product
		Product c1 = new Product("10000", "ACER", 1);
		Product c2 = new Product("10000", "ASUS", 2);
		Product c3 = new Product("10000", "MSI", 3);
		Product c4 = new Product("10000", "PREDATOR", 4);

		// add product into arrayList
		computer.add(c1);
		computer.add(c2);
		computer.add(c3);
		computer.add(c4);

		System.out.println("List computer product : " + computer);
		System.out.println("Please input your ID to delete(1-4) : ");
		int computerId = input.nextInt();

		for (int id = 0; id <= computer.size(); id++) {
			if (computerId == id) {
				computer.remove(id - 1);

				System.out.println("ID computer : " + computerId + " is deleted.");
				System.out.println("List computer Product : " + computer);
			}
		}

		System.out.println("");

		System.out.println("Do you want to add ID ? (Y or N) ");
		String ans = input.next();
		String ans2 = ans.toUpperCase(); // เผื่อไว้ในกรณีที่ user ใส่พิมพ์เล็ก
		// System.out.println(ans2);

		if (ans2.equals("Y")) {
			System.out.println("Please input your ID to add : ");
			int computerId2 = input.nextInt(); // เรียกใช้ method add
			addID(computerId2);
		} else if (ans2.equals("N")) {
			System.out.println("Thank you!");
		}

	}

	public static void addID(int numberId) {
		for (int id = 0; id < numberId; id++) {
			computer.add(id + 1, null);
		}
		System.out.println("ID computer : " + numberId + " to add.");
		System.out.println("List computer Product : " + computer);

	}

}
